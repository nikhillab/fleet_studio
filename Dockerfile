From python:3.8.7

RUN echo "$PWD"
COPY / fleet_studio

WORKDIR /fleet_studio

RUN echo "$PWD"

RUN pip install -r requirements.txt

RUN python manage.py migrate

CMD ["python","manage.py","runserver", "0.0.0.0:8001"]