from rest_framework import serializers
from .models import *
import re
from django.core.validators import validate_email

class ReadUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", 'username', 'email', 'mobile_number')

class UserSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField()                

    class Meta:
        model = User
        fields = ("id", 'username', 'password', 'confirm_password', 'email', 'mobile_number')

    def validate(self, attrs):
        special_sym =["_", "-", "/"]
        
        if not re.match('^[6789]\d{9}$', attrs['mobile_number']):
            raise serializers.ValidationError('Invalid phone number.')

        if  attrs["password"] != attrs["confirm_password"]:
            raise serializers.ValidationError('Password not matched.')

        if len(attrs['password']) > 6:
            raise serializers.ValidationError("Make sure your password is at least 6 letters")

        if not any(char in special_sym for char in attrs['password']): 
            raise serializers.ValidationError('Password should contain atleast: underscore or hyphen or slash.')

        if re.search('[0-9]',attrs['password']) is None:
            raise serializers.ValidationError("Make sure your password has a number in it")

        if re.search('[a-zA-Z]',attrs['password']) is None: 
            raise serializers.ValidationError("Make sure your password has a character in it")

        return attrs

class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()                
    password = serializers.CharField()                

