

def validate_registration_view(request, jsonObj):
    username = jsonObj.get('username')
    email = jsonObj.get('email').strip()
    phone_number = jsonObj.get('phone_number')
    password1 = jsonObj.get('password1')
    password2 = jsonObj.get('password2')

    new_data = {}

    if not username:
        return False, new_data, "Username is required"

    if email:
        try:
            validate_email(email)
        except Exception as e:
            return False, new_data, e.message

    # args = Q()
    # args = args | Q(username=email) | Q(email=email)
    # if User.objects.filter(*(args,)).exists():
    #     return False, new_data, "User with same email already exists."

    if not password1:
        return False, new_data, "Please enter a password."

    if len(password1) < 6:
        return False, new_data, "Password should be greater than 6 characters."

    if not password2:
        return False, new_data, "Please enter confirmation password."

    if password1 != password2:
        return False, new_data, "Entered passwords did not matched."

    new_data.update({
        "first_name": first_name,
        "last_name": last_name,
        "email": email,
        "password1": password1,
        "password2": password2
    })
    return True, new_data, ""
