# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import User


from rest_framework import viewsets, status
from . import serializers as srl
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth import login, authenticate, logout
from django.http import JsonResponse
from .models import User
from rest_framework.permissions import IsAuthenticated


def response_ok(message='OK', status=200):
    return JsonResponse({
        'success': True,
        'message': message
    }, status=status)

def bad_request(message='Bad request', status=400):
    return JsonResponse({
        'success': False,
        'message': message
    }, status=status)


class UserView(viewsets.ModelViewSet):

    def get_queryset(self):
        return User.objects.all()

    def get_serializer_class(self):
        if self.action=="login":
            return srl.UserLoginSerializer
        if self.action=="read_profile":
            return srl.ReadUserSerializer
        return srl.UserSerializer

    @action(detail=False, methods=['post'], url_path='signup')
    def signup(self, request):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return bad_request(serializer.errors)

        data = serializer.data

        user, _ = User.objects.get_or_create(username=data['username'], defaults={
            "email":data['email'],
            "mobile_number":data['mobile_number']
        })

        user.set_password(data['password'])
        user.save()

        return response_ok("Added successfully", status=201)


    @action(detail=False, methods=['post'], url_path='login')
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return bad_request(serializer.errors)

        data = serializer.data

        try:
            username = data['username']
            password = data['password']
        except Exception as e:
            return bad_request("Invalid Request")

        try:
            user = authenticate(username=username, password=password)
        except User.MultipleObjectsReturned as e:
            return Response({"validation": e.message, "status": False})
        if not user:
            return bad_request("Invalid username or password.", status=status.HTTP_401_UNAUTHORIZED)

        login(request, user)
        isAuthenticated = request.user.is_authenticated
        return Response({"validation": "Logged in successfully.", "isAuthenticated": isAuthenticated, "status": True})

    @action(detail=False, methods=['get'], url_path='logout')
    def logout(self, request):
        logout(request)
        return response_ok("Logged out.")

class UserProfileView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return User.objects.all()

    def get_serializer_class(self):
        return srl.ReadUserSerializer

    @action(detail=False, methods=['get'], url_path='read')
    def read_profile(self, request):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)
