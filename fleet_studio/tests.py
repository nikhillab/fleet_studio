from django.test import TestCase
from rest_framework import status
from .models import User

class UserViewTest(TestCase):

    def test_signup_correct(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran-12", "confirm_password":"ran-12", "mobile_number":"7875601282", "email":"nikhilkhadse1993@gmail.com"})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_sigup_wrong_username_length(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2345677", "password":"ran-12", "confirm_password":"ran-12", "mobile_number":"7875601282", "email":"nikhilkhadse1993@gmail.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sigup_wrong_password_match(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran-12", "confirm_password":"ran12", "mobile_number":"7875601282", "email":"nikhilkhadse1993@gmail.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sigup_wrong_password_length(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran-123", "confirm_password":"ran-123", "mobile_number":"7875601282", "email":"nikhilkhadse1993@gmail.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sigup_wrong_password_format(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran123", "confirm_password":"ran123", "mobile_number":"7875601282", "email":"nikhilkhadse1993@gmail.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sigup_wrong_email(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran123", "confirm_password":"ran123", "mobile_number":"7875601282", "email":"nikhilkhadse1993.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_sigup_wrong_mobile_number(self):
        response = self.client.post('/api/v1/user/signup/', { "username":"fleet2", "password":"ran123", "confirm_password":"ran123", "mobile_number":"787560128", "email":"nikhilkhadse1993.com"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class SigninTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='test', password='ran-12')
        self.user.save()

    def test_signin_correct(self):
        response = self.client.post('/api/v1/user/login/', { "username":"test", "password":"ran-12"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_signin_wrong(self):
        response = self.client.post('/api/v1/user/login/', { "username":"test11", "password":"ran-12"})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProfileTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='ran-12')
        self.user.save()
        self.client.force_login(self.user)
    
    def test_read_profile_correct(self):
        response = self.client.get('/api/v1/profile/read/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_read_profile_wrong(self):
        self.client.logout()
        response = self.client.get('/api/v1/profile/read/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    



    